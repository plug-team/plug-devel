#!/usr/bin/env bash

#check gradle version
#for f in $(find ./* -name 'settings.gradle'); do pushd $(dirname $f); ./gradlew --version; popd; done

#update gradle version
#for f in $(find ./* -name 'settings.gradle'); do pushd $(dirname $f); ./gradlew wrapper --gradle-version 5.0 --distribution-type bin; popd; done

#for f in $(find ./* -name '.git'); do pushd $(dirname $f); basename $f; git tag -a v0.0.7 -m "version v0.0.7"; popd; done

#for f in $(find ./* -name '.git'); do pushd $(dirname $f); basename `dirname $f`;git push origin --tags; popd; done

#git push origin --tags


# To publish

#change plug-plugin version number
#and bintrayUpload it

#export PLUG_REPOSITORY=/Users/ciprian/Playfield/repositories/plugTEAM/plug-repository
#cd plug-devel
#./gradlew cleanAll

#run publishAll until no more errors
#./gradlew publishAll

#./gradlew bintrayUploadAll

for f in $(find ./* -name '.git'); do pushd $(dirname $f); basename $f; git tag -a v0.0.8 -m "version v0.0.8"; popd; done

for f in $(find ./* -name '.git'); do pushd $(dirname $f); basename `dirname $f`;git push origin --tags; popd; done

#update the plug-obp site

#export DAILY