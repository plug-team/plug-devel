#!/bin/bash

root=`pwd`/..
modules=(plug-utils plug-utils-fx plug-core jicpl plug-runtime-buchi plug-runtime-buchikripke plug-core-ui plug-pastfreeze plug-synchronization ClockRDL plug-runtime-lclockrdl aefd2fiacre plug-runtime-lexplicit plug-obp-bridge plug-runtime-lspinja plug-runtime-composite plug-runtime-lstate_event plug-runtime-random plug-runtime-ltuml plug-runtime-remote plug-core-ui plug-runtime-ltla)

cd $root
for ix in ${!modules[*]}
do
	cd ${modules[$ix]}
	echo `pwd`
	./gradlew bintrayUpload --refresh-dependencies
	sleep 5s
	cd ..
done

DAILY=true
cd plug-packaging
./gradlew bintrayUpload --refresh-dependencies